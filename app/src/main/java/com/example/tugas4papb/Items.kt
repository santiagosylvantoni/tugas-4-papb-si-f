package com.example.tugas4papb

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Items  (
    val imageHero: Int,
    val nameHeroes: String,
    val subnameHeroes: String
) : Parcelable